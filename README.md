# about

try to migrate from sqlite (origin gitea 1.3.x installation (but fequently updated and running 1.5.0 atm)) to MariaDB

summary: it works (for me). even the tow factor authentication survived  
  * https://git.osuv.de/m/gitea-mariadb-migration
  * mirror: https://gitlab.com/markuman/gitea-mariadb-migration

the `filtered_init.ddl` file was taken from a fresh gitea 1.5.0 installation.

```
mysqldump -h 127.1 -P 3307 -u root -ppassword --no-data gitea | grep --invert-match -E '\/\*|\--' > filtered_init.ddl
```

# requirements

* gitea 1.5.0 - **this is very important. no other version is supported**
* python 3 (only tested with python 3.7)
* make (`make install` will do `pip3 install -t $(pwd) mariasql`)




```
git clone https://git.osuv.de/m/gitea-mariadb-migration
cd gitea-mariadb-migration

# define sqlite3 database and mariadb/mysql credentials
export SQLITE_FILE=~/gitea/data/gitea.db
export MARIADB_PASSWORD=yourpassword
export MARIADB_USER=your_gitea_sql_user
export MARIADB_DB=your_new_gitea_database
export MARIADB_HOST=127.0.0.1
export MARIADB_PORT=3306

# install python dependencies
make install

# stop your gitea instance
# change custom/conf/app.ini to use mysql 

# run
python3.7 migrate

# the warnings comes because the init script tries to drop 
# the tables if they exists. that's not a problem, you can ignore it :)
...
  self._do_get_result()
/home/gitea/git/gitea-mariadb-migration/pymysql/cursors.py:329: Warning: (1051, "Unknown table 'org_user'")
  self._do_get_result()
/home/gitea/git/gitea-mariadb-migration/pymysql/cursors.py:329: Warning: (1051, "Unknown table 'protected_branch'")
  self._do_get_result()
/home/gitea/git/gitea-mariadb-migration/pymysql/cursors.py:329: Warning: (1051, "Unknown table 'public_key'")
  self._do_get_result()
...
start insert data
copy content of table access
copy content of table access_token
copy content of table action
...

# start your gitea instance
```


