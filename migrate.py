#!/usr/bin/python3
import sqlite3
import os
import mariasql
import hashlib
import binascii
import random

def GetRandomString(N):
    return ''.join(random.choice("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz") for _ in range(N))

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

SQLITE_FILE = os.environ.get('SQLITE_FILE')
SQLITE = sqlite3.connect(SQLITE_FILE)
SQLITE.row_factory = dict_factory
SQLITE_CUR = SQLITE.cursor()

MARIADB_HOST = os.environ.get('MARIADB_HOST')
MARIADB_USER = os.environ.get('MARIADB_USER')
MARIADB_PASSWORD = os.environ.get('MARIADB_PASSWORD')
MARIADB_PORT = int(os.environ.get('MARIADB_PORT') or 3306)
MARIADB_DB  = os.environ.get('MARIADB_DB')
MARIADB = mariasql.MariaSQL(
            host=MARIADB_HOST,
            port=MARIADB_PORT,
            user=MARIADB_USER,
            password=MARIADB_PASSWORD,
            db=MARIADB_DB,
            charset='utf8mb4')

"""
with open('init.ddl','r') as inserts:
	sqlScript = inserts.readlines()
	for statement in sqlScript.split(';'):
		MARIADB.query(statement)
"""

print("creating tables")
with open('filtered_init.ddl', 'r') as myfile:
	data = myfile.read().replace('\n', '')
	for statement in data.split(';'):
		if len(statement) > 0:
			output = MARIADB.query(statement)



print("start insert data")
response = SQLITE_CUR.execute("SELECT tbl_name FROM sqlite_master WHERE type = 'table' AND name != 'sqlite_sequence' order by tbl_name;")
tables = response.fetchall()
for table in tables:
	this_table = table['tbl_name']
	print("copy content of table " + this_table)
	for row in SQLITE_CUR.execute("select * from %s" % this_table):
		# i guess the field was renamed
		if this_table == 'comment' and 'old_assignee_id' in row:
			row['removed_assignee'] = row.pop('old_assignee_id')
		# i guess the duplicated information is dropped now, because it is kept in table issue_assignees
		if this_table == 'issue' and 'assignee_id' in row:
			row.pop('assignee_id')
		# i guess the information is dropped, because it is duplicated information that can be
		# find out with an inner join on the issue_assignees. when no result, issue is not assigned
		# futhermore, one issue can hold multiple assignees with this change
		if this_table == 'issue_user' and 'is_assigned' in row:
			row.pop('is_assigned')
		# dunno
		if this_table == 'org_user' and 'is_owner' in row:
			row.pop('is_owner')
		if this_table == 'org_user' and 'num_teams' in row:
			row.pop('num_teams')
		# i guess this information is stores in team_unit table now
		if this_table == 'team' and 'unit_types' in row:
			row.pop('unit_types')
		# migrate scratch token to salt and hash
		# https://github.com/go-gitea/gitea/blob/adf3f004b65135e9375ae60dfd0d9ecba340342e/models/migrations/v71.go#L43
		# https://github.com/go-gitea/gitea/pull/4331
		if this_table == 'two_factor' and 'scratch_token' in row:
			ScratchSalt = GetRandomString(10)
			raw_ScratchHash = hashlib.pbkdf2_hmac('sha256', row['scratch_token'].encode(), ScratchSalt.encode(), 100000)
			ScratchHash = binascii.hexlify(raw_ScratchHash).decode('utf-8')
			row.pop('scratch_token')
			row['scratch_salt'] = ScratchSalt
			row['scratch_hash'] = ScratchHash
		output = MARIADB.insert(this_table, row)
